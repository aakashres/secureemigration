"""SecureEmigration URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from user.views import HomeView, MainView
from manpower.views import ReportDetailView, ReportListView, ReportDeleteView, ReportVerifyView
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', MainView.as_view(), name="main"),
    url(r'^home/$', HomeView.as_view(), name="home"),
    url(r'^report/(?P<id>[0-9]+)/$', ReportDetailView.as_view(), name="report_detail"), 
    url(r'^report/(?P<id>[0-9]+)/$', ReportDeleteView.as_view(), name="delete_report"),
    url(r'^report/(?P<id>[0-9]+)/$', ReportVerifyView.as_view(), name="verify_report"),
    url(r'^report/list/$', ReportListView.as_view(), name="report_list"), 
    url(r'^admin/', admin.site.urls),
    url(r'^user/', include('user.urls', namespace="user")),
    url(r'^manpower/', include('manpower.urls', namespace="manpower")),
    url(r'^account/', include('accounts.urls', namespace="accounts")),
]



if settings.DEBUG:
	urlpatterns  += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
	
