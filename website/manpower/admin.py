from django.contrib import admin

from .models import Manpower, Review, Report

# Register your models here.

admin.site.register(Manpower)
admin.site.register(Review)
admin.site.register(Report)
