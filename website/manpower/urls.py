from django.conf.urls import url

from .views import *

urlpatterns = [
	url(r'^list/$', ListView.as_view(), name="list_manpower"),
	url(r'^add/$', CreateView.as_view(), name="add_manpower"),
	url(r'^(?P<slug>[\w-]+)/$', DetailView.as_view(), name="show_manpower"),
	url(r'^(?P<slug>[\w-]+)/update/$', UpdateView.as_view(), name="update_manpower"),
	url(r'^(?P<slug>[\w-]+)/delete/$', DeleteView.as_view(), name="delete_manpower"),
	url(r'^(?P<slug>[\w-]+)/block/$', BlockView.as_view(), name="block_manpower"),
	url(r'^(?P<slug>[\w-]+)/report/$', ReportView.as_view(), name="report_manpower"),
]



