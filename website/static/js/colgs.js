function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}

function initMap() {

var myLatLng = {lat: 39.5728643, lng: -96.5111808};

var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 4,
  center: myLatLng
});

var locations = [
  [1,'Massachusetts Institute of Technology',42.3601,-71.0921],
  [2,'Stanford University',37.43175,-122.182],
  [3,'University of California - Berkeley',37.87152,-122.26],
  [4,'Carnegie Mellon University',40.444616,-79.942914],
  [5,'California Institute of Technology',34.139275,-118.12574],
  [6,'Georgia Institute of Technology',33.77373,-84.396],
  [7,'Purdue University - West Lafayette',40.42821,-86.9144],
  [8,'University of Illinois - Urbana-Champaign',40.10886,-88.2303],
  [9,'University of Michigan - Ann Arbor',42.27617,-83.7431],
  [10,'University of Southern California (Viterbi)',34.02106,-118.284],
  [11,'University of Texas - Austin (Cockrell)',30.28473,-97.7373],
  [12,'Texas A&M University - College Station (Look)',30.61323,-96.3403],
  [13,'Cornell University',42.445291,-76.48269],
  [14,'Columbia University (Fu Foundation)',40.808286,-73.961885],
  [15,'University of California - Los Angeles (Samueli)',34.06889,-118.444],
  [16,'University of Wisconsin - Madison',43.07386,-89.4054],
  [17,'University of California - San Diego (Jacobs)',32.8766,-117.238],
  [18,'Princeton University',40.3485,-74.6637],
  [19,'University of Pennsylvania',39.951,-75.1936],
  [20,'Harvard University',42.37443,-71.1182],
  [21,'Northwestern University (McCormick)',42.05838,-87.6737],
  [22,'Virginia Tech',37.22856,-80.4233],
  [23,'University of California - Santa Barbara',34.4154,-119.848],
  [24,'University of Maryland - College Park (Clark)',38.98861,-76.9397],
  [25,'Johns Hopkins University (Whiting)',39.32838,-76.6208],
  [26,'Pennsylvania State University - University Park',40.80073,-77.8616],
  [27,'University of Washington',47.65621,-122.313],
  [28,'Duke University (Pratt)',36.00159,-78.94226],
  [29,'North Carolina State University',35.78511,-78.6745],
  [30,'University of Minnesota - Twin Cities',44.97789,-93.2354],
  [31,'Rice University (Brown)',29.71649,-95.4036],
  [32,'Ohio State University',39.99839,-83.009],
  [33,'University of California - Davis',38.5399,-121.752],
  [34,'University of Colorado - Boulder',40.00442,-105.267],
  [35,'Vanderbilt University',36.14443,-86.8049],
  [36,'Yale University',41.31116,-72.9267],
  [38,'University of California - Irvine (Samueli)',33.64843,-117.841],
  [39,'Rensselaer Polytechnic Institute',42.72898,-73.6788],
  [40,'University of Rochester',43.12644,-77.631],
  [41,'University of Virginia',38.03655,-78.5026],
  [42,'Arizona State University (Tempe )',33.421922,-111.940011],
  [43,'Iowa State University',42.02637,-93.6484],
  [44,'Northeastern University',42.33999,-71.0888],
  [45,'University of Florida',29.63825,-82.3612],
  [46,'University of Pittsburgh (Pittsburgh)',40.4445,-79.9547],
  [47,'Case Western Reserve University',41.500812,-81.605769],
  [48,'New York University',40.72945,-73.9973],
  [49,'Brown University',41.827605,-71.404467],
  [50,'Lehigh University (Rossin)',40.60716,-75.3789],
  [51,'Michigan State University',42.72476,-84.4736],
  [52,'University of Arizona',32.23207,-110.951],
  [53,'University of Notre Dame',41.70306,-86.239],
  [54,'University of Delaware',39.67979,-75.7536],
  [55,'Washington University in St. Louis',38.64813,-90.3116],
  [56,'Colorado School of Mines',39.75079,-105.227148],
  [57,'Rutgers, The State University of New Jersey - New Brunswick',40.5038,-74.4505],
  [58,'University of Utah',40.76248,-111.846],
  [59,'University at Buffalo - SUNY',42.9342,-78.8807],
  [60,'University of Massachusetts - Amherst',42.38611,-72.5261],
  [61,'Dartmouth College (Thayer)',43.70408,-72.289951],
  [62,'University of Illinois - Chicago',41.87265,-87.6511],
  [63,'University of Dayton',39.73686,-84.1737],
  [64,'University of Iowa',41.6607,-91.5357],
  [65,'University of Tennessee - Knoxville',35.95509,-83.9297],
  [66,'Drexel University',39.954855,-75.188745],
  [67,'Stony Brook University - SUNY',40.91314,-73.1236],
  [68,'Auburn University (Ginn)',32.600201,-85.492409],
  [69,'Tufts University',42.406,-71.1206],
  [70,'University of Connecticut',41.80821,-72.2495],
  [71,'Clemson University',34.677329,-82.834463],
  [72,'Colorado State University',40.574759,-105.08083],
  [73,'University of California - Riverside (Bourns)',33.97588,-117.331],
  [74,'Illinois Institute of Technology',41.8338,-87.6283],
  [75,'Syracuse University',43.04053,-76.1367],
  [76,'Oregon State University',44.56274,-123.275],
  [77,'Stevens Institute of Technology (Schaefer)',40.74478,-74.0253],
  [78,'University of Houston',29.71999,-95.3449],
  [79,'University of Texas - Dallas (Jonsson)',32.98943,-96.7484],
  [81,'Missouri University of Science & Technology',37.95585,-91.7759],
  [82,'University of Cincinnati',39.13116,-84.5143],
  [84,'University of North Carolina - Chapel Hill',35.91217,-79.051],
  [85,'University of Central Florida',28.60106,-81.1988],
  [86,'University of New Mexico',35.08387,-106.62],
  [87,'Worcester Polytechnic Institute',42.27507,-71.8088],
  [88,'University of California - Santa Cruz (Baskin)',36.99726,-122.068],
  [89,'University of Missouri',38.94102,-92.3264],
  [89,'Washington State University',46.73045,-117.158],
  [90,'University of Alabama - Huntsville',34.72282,-86.6384],
  [91,'University of Kansas',38.95735,-95.2459],
  [92,'University of Nebraska - Lincoln',40.82227,-96.7022],
  [93,'University of Texas - Arlington',32.72844,-97.1151],
  [94,'Kansas State University',39.18862,-96.5811],
  [95,'Louisiana State University - Baton Rouge',30.55211,-91.1967],
  [96,'Mississippi State University (Bagley)',33.45524,-88.7883],
  [97,'Texas Tech University',33.58031,-101.877],
  [98,'University of Kentucky',38.03239,-84.5092],
  [99,'George Washington University',38.89865,-77.0479],
  [100,'University of Oklahoma',35.21,-97.45],
  [101,'University of South Carolina',33.99897,-81.027],
  [102,'University of South Florida',28.05665,-82.4159],
  [103,'Wichita State University',37.71648,-97.2967],
  [104,'Indiana University - Purdue University - Indianapolis',39.77295,-86.1722],
  [105,'Michigan Technological University',47.1183,-88.5464],
  [106,'University of Alabama',33.2144,-87.5458],
  [107,'George Mason University',38.83088,-77.3079],
  [108,'Tulane University',29.9397,-90.1204],
  [109,'University of Arkansas - Fayetteville',36.06153,-94.1782],
  [110,'West Virginia University',39.63468,-79.9539],
  [111,'Clarkson University',44.663761,-74.999947],
  [112,'CUNY - City College (Grove)',40.819794,-73.95055],
  [113,'New Jersey Institute of Technology',40.742,-74.1771],
  [114,'Southern Methodist University',32.84525,-96.7849],
  [115,'University of Miami',25.72041,-80.2765],
  [116,'University of Tulsa',36.15189,-95.9452],
  [117,'Baylor University',31.545956,-97.119359],
  [118,'Binghamton University - SUNY (Watson)',42.088049,-75.971143],
  [119,'Brigham Young University',40.250851,-111.649281],
  [120,'Florida A&M University - Florida State University',30.4421,-84.294801],
  [121,'University of Akron',41.07777,-81.5103],
  [122,'University of Maryland - Baltimore County',39.25669,-76.7116],
  [123,'University of Massachusetts - Lowell (Francis)',42.65372,-71.3251],
  [124,'Wayne State University',42.35861,-83.0729],
  [126,'University of Louisville',38.21575,-85.7588],
  [128,'New Mexico State University',32.28288,-106.748],
  [129,'Temple University',39.98094,-75.1583],
  [130,'University of Maine',44.89597,-68.6738],
  [131,'University of Nevada - Reno',39.54935,-119.822],
  [132,'University of North Carolina - Charlotte',35.30683,-80.7358],
  [133,'University of Wisconsin - Milwaukee',43.07685,-87.8805],
  [134,'University of Wyoming',41.31194,-105.572],
  [135,'Louisiana Tech University',32.52552,-92.6496],
  [136,'University of Georgia',33.95643,-83.374],
  [137,'University of New Hampshire',43.13424,-70.9345],
  [138,'Wright State University',39.78063,-84.0649],
  [139,'Marquette University',43.03877,-87.9281],
  [140,'Ohio University-Main Campus',39.3268,-82.1011],
  [141,'Oklahoma State University',36.12309,-97.0697]
]

var marker, i;
var redCircle ={
    path: google.maps.SymbolPath.CIRCLE,
    fillColor: 'red',
    fillOpacity: 1.0,
    scale: 4.0,
    strokeColor: 'black',
    strokeWeight: 1
};
var greenCircle ={
    path: google.maps.SymbolPath.CIRCLE,
    fillColor: 'green',
    fillOpacity: 1.0,
    scale: 4.0,
    strokeColor: 'black',
    strokeWeight: 1
};
var yellowCircle ={
    path: google.maps.SymbolPath.CIRCLE,
    fillColor: 'yellow',
    fillOpacity: 1.0,
    scale: 4.0,
    strokeColor: 'black',
    strokeWeight: 1
};

for (i = 0; i < locations.length; i++) {
    var circle;
    if(i>=0)
      circle = redCircle;
    if(i>=25)
      circle = yellowCircle;
    if(i>=75)
      circle = greenCircle;

    title = "#" + locations[i][0] + " " + locations[i][1];

    marker = new google.maps.Marker({
      position: {lat: locations[i][2], lng: locations[i][3]},
      map: map,
      icon: circle,
      title: title
    });
    contentString = '<div id="content">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<a href="colleges/'+
          convertToSlug(locations[i][1])+
          '">'+
          '<h1 id="firstHeading" class="firstHeading">'+
          locations[i][1]+
          '</h1>'+
          '</a> '+
          '</div>';

    infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i, contentString, infowindow) {
      return function() {
        infowindow.setContent(contentString);
          infowindow.open(map, marker);
      }
    })(marker, i, contentString, infowindow));
}

}